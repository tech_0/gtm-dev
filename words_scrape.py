# https://www.falconliving.com/property/10116759/

from bs4 import BeautifulSoup
from urllib.request import urlopen
import requests
import csv


def scrape_property(url,check_lst):
	#url = "https://www.falconliving.com/property/10116759/"
	page = urlopen(url)
	soup = BeautifulSoup(page,'html.parser')
	[s.extract() for s in soup('script')]

	lst=[]
	#########
	#prop_address = soup.find('div',{'class':'prop-address'})
	#prop_address = prop_address.find('h1')
	#prop_address = prop_address.text
	#lst.append(url+" --> "+prop_address)
	#check_lst.append(url+"->"+prop_address)
	########
	top_options = soup.find('div',{'class':'inner'})
	top_options = top_options.findAll('span')

	flag=0
	for opt in top_options:
		flag+=1
		opt_txt_ = (opt.text).split(",")
		for opt_txt in opt_txt_:
			if (opt_txt not in check_lst):
				lst.append(opt_txt)
				check_lst.append(opt_txt)
		if flag==3:
			break
	#########
	prop_details = soup.find('div',{'class':'prop-sup-details'})
	prop_details = prop_details.findAll("dt")
	for detail in prop_details:
		txt_ = ((detail.text).strip()).split(',')
		for txt in txt_:
			if not (txt == "" or txt==" "):
				if txt not in check_lst:
					lst.append(txt)
					check_lst.append(txt)
	############
	sub_bar = soup.find('div', {'class': 'sub-bar'})
	sub_bar = sub_bar.findAll('a')
	for sub in sub_bar:
		sub_ = (sub.text).split(",")
		for sub_tmp in sub_:
			if sub_tmp not in check_lst:
				lst.append(sub_tmp)
				check_lst.append(sub_tmp)
	############
	if 'View Gallery' not in check_lst:
		lst.append('View Gallery')
		check_lst.append('View Gallery')
	############
	prop_highlight = soup.find('div', {'class': 'prop-descrip'})
	prop_highlight = prop_highlight.findChildren("dt")[0]
	prop_highlight_ = (prop_highlight.text).split(",")
	for prop_highlight in prop_highlight_:
		if prop_highlight not in check_lst:
			lst.append(prop_highlight)
			check_lst.append(prop_highlight)

	prop_details = soup.find('dl', {'class': 'property-details-section'})
	prop_details_dt = prop_details.findAll('dt')
	prop_details_dd = prop_details.findAll('dd')
	temp=[]
	for dt,dd in zip(prop_details_dt,prop_details_dd):
		#print(dt,dd)
		if dt.text not in check_lst:
			lst.append(dt.text)
			check_lst.append(dt.text)
		if dt.text=="Status" or dt.text=="Subtype" or dt.text=="Type":
			if dd.text not in check_lst:
				lst.append(dd.text)
				check_lst.append(dd.text)
	#########
	sub_bar = (soup.findAll('div', {'class': 'sub-bar'}))[2]
	sub_bar = sub_bar.findAll('a')
	for sub in sub_bar:
		sub_ = (sub.text).split(",")
		for sub_tmp in sub_:
			if sub_tmp not in check_lst:
				lst.append(sub_tmp)
				check_lst.append(sub_tmp) #################
	##########
	add_info = soup.find('div', {'class': 'additional-information-element'})
	add_info = add_info.find('h3')
	add_info_ = (add_info.text).split(",")
	for add_info in add_info_:
		if add_info not in check_lst:
			lst.append(add_info)
			check_lst.append(add_info)
	########
	comp_stamp = soup.find('div',{'class':'company-stamp'})
	comp_stamp = comp_stamp.findAll('a')
	for i in comp_stamp:
		tmp_txt_ = ((i.text).strip()).split(",")
		for tmp_txt in tmp_txt_:
			if tmp_txt not in check_lst:
				lst.append(tmp_txt)
				check_lst.append(tmp_txt)
	########
	tmp_txt = "Additional Information"
	if tmp_txt not in check_lst:
		lst.append(tmp_txt)
		check_lst.append(tmp_txt)
	########
	prop_sec = soup.findAll('dl',{'class':'property-details-section'})[1]
	prop_sec = prop_sec.findAll('div',{'class':'property-details-column'})
	prop_sec = [prop_sec[0].findAll('dt'),prop_sec[0].findAll('dd')],[prop_sec[1].findAll('dt'),prop_sec[1].findAll('dd')]
	for col in prop_sec:
		for dt,dd in zip(col[0],col[1]):
			dt_txt_ = (dt.text).split(",")
			dd_txt_ = (dd.text).split(",")
			for dt_txt in dt_txt_:
				if dt_txt not in check_lst:
					lst.append(dt_txt)
					check_lst.append(dt_txt)
			for dd_txt in dd_txt_:
				if dd_txt not in check_lst:
					lst.append(dd_txt)
					check_lst.append(dd_txt) 
	########
	mort_calc = soup.find('div',{'class':'mortgage_calculator'})
	tmp_txt = (mort_calc.find('h3').text)
	if tmp_txt not in check_lst:
		lst.append(tmp_txt)
		check_lst.append(tmp_txt)
	mort_dt = mort_calc.findAll('dt')
	mort_dd = mort_calc.findAll('dd')
	for dt in mort_dt:
		tmp_txt = (dt.text).strip()
		if tmp_txt not in check_lst:
			lst.append(tmp_txt)
			check_lst.append(tmp_txt)
	for dd in mort_dd:
		tmp_txt = (dd.text).strip()
		if tmp_txt not in check_lst:
			lst.append(tmp_txt)
			check_lst.append(tmp_txt)
	mort_divs = mort_calc.findAll('div')
	for div in mort_divs:
		tmp_txt=(div.text).strip()
		if tmp_txt not in check_lst:
			lst.append(tmp_txt)
			check_lst.append(tmp_txt)
	############
	return(lst,check_lst)


def write_csv(lst):
	with open("output01.csv",'a',newline='') as resultFile:
		wr = csv.writer(resultFile, dialect='excel')
		for lst_ in lst:
			if not (lst_.isdigit() or lst_.startswith("$")):
				flag=1
				try:
					if (lst_[0].isdigit() and lst_[2]=='X'):
						flag=0
				except:
					flag=1
				if flag:
					lst_=lst_.strip()					
					lst_=[lst_]
					#print(lst_)
					wr.writerow(lst_)


def driver():
	check_lst=[]
	for i in range(1,2001):
		url = "https://www.falconliving.com/search/details/7j/"+str(i)
		try:
			lst,check_lst=scrape_property(url,check_lst)
		except:
			print("Error")
		print(i,"---",len(lst),"---",len(check_lst))
		print("\n")
		#print(check_lst)
		write_csv(lst)


driver()







